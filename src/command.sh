#!/bin/bash

line_heading="Last updated:"
date_line="$line_heading $(date +%F)"
commit_message=$2
file_name="README.md"
projects_path="/path/to/git/projects/$1"

# change to projects directory
cd $projects_path

count=$(grep -o -i "$line_heading" $file_name | wc -l)
if [ $count -eq 0 ]; then
  # add line and commit at end of file
  echo "" >> $file_name
  echo "$date_line" >> $file_name
else
  # find and replace line
  sed -e "s/$line_heading.*/$date_line/ig" $file_name > temp_file
  mv temp_file $file_name
fi
  # commit and push
  git add .
  git commit -m "$commit_message"
  git push origin master

exit 0
