import subprocess

from constants import base_path
from functions import (
    get_project_name_random,
    get_commit_message_random,
    log_commit,
)


def run_commit_bot():
    project_name = get_project_name_random()
    commit_message = get_commit_message_random()
    log_commit(project_name, commit_message)

    print(project_name)

    p = subprocess.Popen([
        "bash",
        f"{base_path}/src/command.sh",
        project_name,
        commit_message,
    ], stdout=subprocess.PIPE)

    # end subprocess when return code is 0
    streamdata = p.communicate()[0]
    code = p.returncode

    if code == 0:
        p.kill()


if __name__ == "__main__":
    run_commit_bot()
