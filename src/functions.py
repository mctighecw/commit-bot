import csv
import random
from datetime import datetime

from constants import log_file_path
from info import project_names, commit_messages


def get_timestamp_now():
    """Timestamp now as an ISO string, in current timezone."""
    time_format = "%Y-%m-%dT%H:%M:%SZ"
    return datetime.now().strftime(time_format)


def get_project_name_random():
    """Select a random project using a random integer."""
    total_projects = len(project_names)
    project_index = random.randrange(0, total_projects, 1)

    return project_names[project_index]


def get_commit_message_random():
    """Select a random commit message using a random integer."""
    total_messages = len(commit_messages)
    message_index = random.randrange(0, total_messages, 1)

    return commit_messages[message_index]


def log_commit(project_name, commit_message):
    """Write the commit information to a CSV log file."""
    timestamp = get_timestamp_now()
    data = [timestamp, project_name, commit_message]

    with open(log_file_path, "a", encoding="UTF8", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(data)
