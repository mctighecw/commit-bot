# all projects (value is the local directory name)
project_names = [
  "project-a",
  "project-b",
  "project-c",
  "project-d",
  "project-e",
  "project-f"
]

commit_messages = [
  "Add new date value",
  "Change date",
  "Modify date",
  "Update date",
  "Update readme file"
]
