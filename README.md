# README

Commit Bot app, to automatically make commits to existing Git projects, chosen randomly.

The last line of the `README.md` gets a new "Last updated" value, which is subsequently replaced. A random commit message is chosen.

After each bot commit, a new line is added to the `log_file.csv` (with timestamp, project name, and commit message).

The bot needs to be run manually, but a Shell script alias can be added for convenience.

## App Information

App Name: commit-bot

Created: August-September 2022

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/commit-bot)

## Tech Stack

- Python 3
- Shell

## Configuration

1. Replace `/path/to/...` values with the correct absolute paths in `constants.py` and `command.sh` files

1. Add project names to `project_names` in `info.py`

1. Optional: add/ update `commit_messages` in `info.py`

1. Optional: add Shell alias, for example:

```txt
In ~/.zshrc:

alias commit-bot="python3 /path/to/project/commit-bot/src/bot.py"
```

## To Run

```sh
% python3 /path/to/project/commit-bot/src/bot.py
```

Or with the alias:

```sh
% commit-bot
```

Last updated: 2025-03-02
